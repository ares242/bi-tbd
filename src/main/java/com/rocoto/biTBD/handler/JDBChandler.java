package com.rocoto.biTBD.handler;

import java.sql.*;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class JDBChandler {
	Connection connection = null;
	
	private String driver;
	private String url;
	private String user;
	private String pass;
	
	private static final Logger log = LogManager.getLogger(JDBChandler.class);
	
	public JDBChandler(String driver, String url, String user, String password){
		this.driver = driver;
		this.url = url;
		this.user = user;
		this.pass = password;
		Connect();
	}
	
	public Connection Connect()
	{
		try {
			 
			Class.forName(driver);
 
		} catch (ClassNotFoundException e) {
			log.catching(Level.ERROR, e);
			return connection;
 
		}
		
		try {
			 
			connection = DriverManager.getConnection(url, user,pass);
 
		} catch (SQLException e) {
			log.catching(Level.ERROR, e);
			return connection;
 
		}
 
		if (connection != null) {
			log.info("JDBC direct conection Ok.");
		} else {
			log.info("Failed to make JDBC connection.");
		}
		return connection;
	}

	public Connection getConnection() {
		return connection;
	}

	protected void setConnection(Connection connection) {
		this.connection = connection;
	}
	
}
