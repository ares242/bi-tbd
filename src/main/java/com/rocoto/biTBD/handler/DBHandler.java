package com.rocoto.biTBD.handler;

//import java.sql.Connection;
import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.vaadin.data.util.sqlcontainer.connection.JDBCConnectionPool;
import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;

///TODO use data sources
//import javax.sql.DataSource;

public class DBHandler {

	public static EntityManagerFactory emf = Persistence.createEntityManagerFactory("bi");

	public static EntityManager getNewEntityManager() {
		return emf.createEntityManager();
	}
	
	public static EntityManagerFactory getEntityManagerFactory() {
		return emf;
	}
	
	public static JDBChandler jdbc = new JDBChandler("org.postgresql.Driver",
			"jdbc:postgresql://127.0.0.1:5432/ventasTDB","postgres","123456");
	
	public static JDBCConnectionPool connectionPool ;
	static{
		try {
			connectionPool  = new SimpleJDBCConnectionPool("org.postgresql.Driver",
					"jdbc:postgresql://127.0.0.1:5432/ventasTDB","postgres","123456");
			//Connection con = connectionPool.reserveConnection();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
