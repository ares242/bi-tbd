package com.rocoto.biTBD;

import com.rocoto.biTBD.handler.DBHandler;
import com.rocoto.biTBD.model.ChartAxis;
import com.rocoto.biTBD.model.Hierarchy;
import com.rocoto.biTBD.model.MetaChart;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.*;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;

import java.sql.*;
import java.util.*;
import java.lang.reflect.Field;

public class ChartBuilder {

    private String viewNamePrefix = "precdatachart";
	
	private MetaChart metaChart;
	private Set<String> categories = new HashSet<>();
	private Set<String> seriesNames = new HashSet<>();
	private Map<String, Map<String, Double> > data = new HashMap<>();
	private List<Series> seriesData = new ArrayList<>();
	private String xAxisTitle = null;
	private boolean rotate = false;
	private Boolean forceUpdate = false;
	private Boolean noLegend = false;
	
	private static final Logger log = LogManager.getLogger(ChartBuilder.class);


    private void processResult(ResultSet rs,ResultSetMetaData rsmd ){
        try {
        Integer num_cols = rsmd.getColumnCount();
        if(num_cols==2)
        {
            ListSeries series = new ListSeries(rsmd.getColumnName(2));
            while (rs.next()) {
                categories.add(rs.getString(2));
                series.addData(rs.getDouble(1));
            }
            seriesData.add(series);
            xAxisTitle = rsmd.getColumnName(2);
            noLegend = true;
        }
        else if(num_cols>=3)
        {
            rotate=true;
            while (rs.next()) {
                StringBuilder cats = new StringBuilder(rs.getString(3));
                for(int i = 4;i<=rsmd.getColumnCount();i++)
                    cats.append("-").append(rs.getString(i));
                categories.add(cats.toString());

                seriesNames.add(rs.getString(2));
                Map<String, Double> s = data.get(cats.toString());
                if(s==null)
                    data.put(cats.toString() , s = new HashMap<>());
                s.put(rs.getString(2), rs.getDouble(1));
                //meassures.add(rs.getDouble(1));
            }
            //String seriesPrefix = rsmd.getColumnName(2);
            for(String s : seriesNames)
            {
                ListSeries series = new ListSeries();
                series.setName(s);
                for(String cat : categories)
                {
                    series.addData(data.get(cat).get(s));
                }
                seriesData.add(series);
            }
            xAxisTitle = rsmd.getColumnName(3);
        }
        } catch (SQLException e) {
            log.catching(Level.ERROR, e);
        }
    }

	private void buildData()
	{
		if(metaChart.getChartAxis().isEmpty()){
            return;
        }
		if(metaChart.getSql() ==  null || forceUpdate )
		{ precalculateData();}

        Connection con = DBHandler.jdbc.getConnection();
		try {
			Statement stmt = con.createStatement();
			//String sql = metaChart.computeGetDataSql();
            String sql = "SELECT * FROM "+viewNamePrefix+metaChart.getIdChart();
			log.debug("Executing data query "+sql);
			ResultSet rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
            processResult(rs,rsmd);

		} catch (SQLException e) {
			log.catching(Level.ERROR, e);
		}
	}

    private Chart buidChart(){
        log.trace("building visual chart");

        ChartType cht = null;
        try {
			Field f = ChartType.class.getField(metaChart.getChartType().getName());
			cht = (ChartType) f.get(null);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Chart chart_ = new Chart(cht);
        Configuration conf = chart_.getConfiguration();

        conf.setTitle(metaChart.getChartTitle());
        conf.setSubTitle(metaChart.getComent());

        XAxis x = new XAxis();
        x.setCategories(categories.toArray(new String[categories.size()]));
        if(xAxisTitle!=null)
            x.setTitle(xAxisTitle);
        if(rotate)
        {
            Labels labels = new Labels();
            labels.setRotation(-45);
            labels.setAlign(HorizontalAlign.RIGHT);
            x.setLabels(labels);
        }
        conf.addxAxis(x);

        YAxis y = new YAxis();
        y.setMin(0);
        y.setTitle(metaChart.getCube().getMesureName());
        conf.addyAxis(y);

        Tooltip tooltip = new Tooltip();
        tooltip.setFormatter("this.x +': '+ this.y");
        conf.setTooltip(tooltip);

        if(noLegend)
            conf.getLegend().setEnabled(false);

        //conf.setSeries(seriesData);

        for(Series s :  seriesData )
            conf.addSeries(s);

        chart_.drawChart(conf);

        return chart_;
    }

	public Chart build()
	{
		categories.clear();
        seriesData.clear();
        seriesNames.clear();
        data.clear();
        if(metaChart==null) return null;
        log.trace("building data");
        categories.clear();
        seriesData.clear();
		buildData();
		return buidChart();
	}

    public Chart DrillDown(String seriesName, String category, Collection<Hierarchy> drillDownCols)
    {
        categories.clear();
        seriesData.clear();
        seriesNames.clear();
        data.clear();
        Chart chart_ = new Chart(ChartType.COLUMN);

        //StringBuilder condition = new StringBuilder("WHERE ");
        //condition.append(seriesName).append(" = ").append(category);

        /*if(metaChart.getChartAxis().size()==1)
            Map< Hierarchy,String> where = new HashMap<>();
            where.put(metaChart.getChartAxis().get(0).getHierarchy(),category);*/

        List<String> where = new ArrayList<>();
        if(metaChart.getChartAxis().size()==1)
            where.add(category);
        else
            where.add(seriesName);
        where.addAll(Arrays.asList(category.split("-")));
        
        List<ChartAxis> axis = metaChart.getChartAxis();
        
        StringBuilder filter = new StringBuilder("");
        for(int i = 0; i<axis.size();i++){
            if(i!=0) filter.append(" AND ");
            Hierarchy h = axis.get(i).getHierarchy();
            filter.append(h.getDimension().getDimAlias()).append('.').append(h.getHierarchyAlias())
            .append("=").append(where.get(i));
        }

        String sql = metaChart.computeDrillDownSql(drillDownCols,where);

        System.out.println(sql);

        Connection con = DBHandler.jdbc.getConnection();

        try {
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            processResult(rs,rs.getMetaData());
            chart_ = buidChart();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        chart_.getConfiguration().setSubTitle(filter.toString());
        return chart_;
    }


	private void precalculateData()
	{
		log.debug("Precalculating data from chart id: "+ metaChart.getIdChart());
		Connection con = DBHandler.jdbc.getConnection();
		Statement stmt;
		EntityManager em = DBHandler.getNewEntityManager();
		//MetaChart chart_ = em.find(MetaChart.class, this.metaChart.getIdChart());
		String sql = metaChart.computeSql();
		try {
			stmt = con.createStatement();
            String viewName = viewNamePrefix+metaChart.getIdChart();
            stmt.executeUpdate("DROP MATERIALIZED VIEW IF EXISTS "+viewName);
            StringBuilder createViewQuery = new StringBuilder("CREATE MATERIALIZED VIEW ");
            createViewQuery.append(viewName).append(" AS ")
            .append(sql).append(" WITH DATA;");
            log.debug("Executing query "+ createViewQuery);

			//stmt.executeQuery("select array_to_json(array_agg(row_to_json(t))) from ( " + sql + " ) t");


            long start = System.nanoTime();
            stmt.executeUpdate(createViewQuery.toString());
            long end = System.nanoTime();
            long elapsedTime = end - start;
            double seconds = (double)elapsedTime / 1000000000.0;

            log.debug("Query execution tooks "+seconds +"seconds");
            em.getTransaction().begin();
            metaChart.setSql(sql);
            em.merge(metaChart);
            em.getTransaction().commit();
			/*ResultSet rs = stmt.getResultSet();
			rs.next();
			String json = rs.getString(1);
			em.getTransaction().begin();
			chart_.setSql(sql);
			chart_.setDatasql(chart_.computeGetDataSql());
			Query q = em.createNativeQuery("update metadata.chart set data = ?::json where id_chart = ?");
			q.setParameter(1, json);
			q.setParameter(2, chart_.getIdChart());
			q.executeUpdate();
			em.getTransaction().commit();*/
		} catch (SQLException e) {
			log.catching(e);
		}
	}
	
	public Boolean getForceUpdate() {
		return forceUpdate;
	}

	public void setForceUpdate(Boolean forceUpdate) {
		this.forceUpdate = forceUpdate;
	}

	public MetaChart getChart() {
		return metaChart;
	}

	public void setChart(MetaChart chart) {
		this.metaChart = chart;
	}

	public Set<String> getCategories() {
		return categories;
	}

	public void setCategories(Set<String> categories) {
		this.categories = categories;
	}

	public Set<String> getSeriesNames() {
		return seriesNames;
	}

	public void setSeriesNames(Set<String> seriesNames) {
		this.seriesNames = seriesNames;
	}
	
}
