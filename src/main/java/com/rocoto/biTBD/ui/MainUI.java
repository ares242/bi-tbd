package com.rocoto.biTBD.ui;

import java.util.Spliterator;
import java.util.function.Consumer;

import javax.servlet.annotation.WebServlet;

import com.rocoto.biTBD.views.Cubes;
import com.rocoto.biTBD.views.Report;
import com.rocoto.biTBD.views.ShowChart;
import com.rocoto.biTBD.views.TestChart;
import com.rocoto.biTBD.views.Welcome;
import com.vaadin.addon.charts.ChartOptions;
import com.vaadin.addon.charts.themes.HighChartsDefaultTheme;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@Theme("bi")
@SuppressWarnings("serial")
public class MainUI extends UI {
	
	public final static String context = VaadinServlet.getCurrent().getServletContext().getContextPath();
	
	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = MainUI.class, widgetset = "com.rocoto.biTBD.AppWidgetSet")
	public static class Servlet extends VaadinServlet {
	}

	private VerticalLayout header_layout;
	private HorizontalLayout content_layout;
	private VerticalLayout footer_layout;
	private VerticalLayout view_layout ;
	
	/*static{
		ChartOptions.get().setTheme(new HighChartsDefaultTheme());
	}*/
	
	@Override
	protected void init(VaadinRequest request) {
		content_layout = new HorizontalLayout();
		
		ChartOptions.get().setTheme(new HighChartsDefaultTheme());
		setupNavigator();
		view_layout = ViewLayout_build();
		
		setContent(view_layout);

	}
	
	public VerticalLayout ViewLayout_build (){
		view_layout = new VerticalLayout();
		view_layout.setSizeFull();
		
		header_layout = Header_build();
		content_layout = Content_build();
		footer_layout = Footer_build();
		
		view_layout.addComponent(header_layout);
		view_layout.addComponent(content_layout);
		view_layout.addComponent(footer_layout);
		
		
		view_layout.setExpandRatio(header_layout, 1.0f);
		view_layout.setExpandRatio(content_layout, 18.5f);
		view_layout.setExpandRatio(footer_layout, 0.5f);
		
		
		return view_layout;
	}
	
	
	
	public VerticalLayout Header_build(){
		header_layout = new VerticalLayout();
		header_layout.setSizeFull();
		
		header_layout.setStyleName("bi-header");
		
		return header_layout;
	}
	public HorizontalLayout Content_build(){
		content_layout.setSizeFull();
		content_layout.setStyleName("bi-content");
		return content_layout;
	}
	public VerticalLayout Footer_build(){
		footer_layout = new VerticalLayout();
		footer_layout.setSizeFull();

		footer_layout.setStyleName("bi-footer");
		
		return footer_layout;
	}
	
	

	@Override
	public void forEach(Consumer<? super Component> action) {
		// TODO Auto-generated method stub

	}

	@Override
	public Spliterator<Component> spliterator() {
		// TODO Auto-generated method stub
		return null;
	}

	private void setupNavigator() {
		Navigator navigator = new Navigator(this, content_layout);
		navigator.addView("",Welcome.class);
		navigator.addView(Cubes.class.getSimpleName(),Cubes.class);
		navigator.addView(Report.class.getSimpleName(),Report.class);
		navigator.addView(TestChart.class.getSimpleName(), TestChart.class);
		navigator.addView(ShowChart.class.getSimpleName(), ShowChart.class);
		navigator.setErrorView(Welcome.class);
	}
	
}
