package com.rocoto.biTBD.model;

import java.io.Serializable;

import javax.persistence.*;

//import org.eclipse.persistence.annotations.Cache;

//import javax.persistence.*;


/**
 * The persistent class for the chart_axis database table.
 * 
 */
@Entity
@Table(name="chart_axis",schema="metadata")
@NamedQuery(name="ChartAxi.findAll", query="SELECT c FROM ChartAxis c")
/*@Cache(
		expiry=36000000
	)*/
@Cacheable(false)
public class ChartAxis implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private ChartAxiPK id;

	private String coment;

	private String sqlfiler;

	//bi-directional many-to-one association to Chart
	@MapsId("idChart")
	@JoinColumn(name="id_chart")
	private MetaChart chart;

	//bi-directional many-to-one association to Dimension
	@MapsId("idDim")
	@JoinColumn(name="id_dim")
	private Dimension dimension;

	//bi-directional many-to-one association to Hierarchy
	@MapsId("idHierarchy")
	@JoinColumn(name="id_hierarchy")
	private Hierarchy hierarchy;
	
	private Integer priority;
	
	public ChartAxis() {
	}

	public ChartAxiPK getId() {
		return this.id;
	}

	public void setId(ChartAxiPK id) {
		this.id = id;
	}

	public String getComent() {
		return this.coment;
	}

	public void setComent(String coment) {
		this.coment = coment;
	}

	public String getSqlfiler() {
		return this.sqlfiler;
	}

	public void setSqlfiler(String sqlfiler) {
		this.sqlfiler = sqlfiler;
	}

	public MetaChart getChart() {
		return this.chart;
	}

	public void setChart(MetaChart chart) {
		this.chart = chart;
	}

	public Dimension getDimension() {
		return this.dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

	public Hierarchy getHierarchy() {
		return this.hierarchy;
	}

	public void setHierarchy(Hierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

}