package com.rocoto.biTBD.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the hierarchy database table.
 * 
 */
@Entity
@Table(schema="metadata")
@NamedQuery(name="Hierarchy.findAll", query="SELECT h FROM Hierarchy h")
public class Hierarchy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="HIERARCHY_IDHIERARCHY_GENERATOR", schema="metadata", sequenceName="ID_HIERARCHY_SEQ",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="HIERARCHY_IDHIERARCHY_GENERATOR")
	@Column(name="id_hierarchy")
	private Long idHierarchy;

	@Column(name="hierarchy_alias")
	private String hierarchyAlias;

	private String ptablefield;

	//bi-directional many-to-one association to ChartAxi
	@OneToMany(mappedBy="hierarchy")
	private List<ChartAxis> chartAxis;

	//bi-directional many-to-one association to Dimension
	@ManyToOne
	@JoinColumn(name="id_dimesion")
	private Dimension dimension;

	public Hierarchy() {
	}

	public Long getIdHierarchy() {
		return this.idHierarchy;
	}

	public void setIdHierarchy(Long idHierarchy) {
		this.idHierarchy = idHierarchy;
	}

	public String getHierarchyAlias() {
		return this.hierarchyAlias;
	}

	public void setHierarchyAlias(String hierarchyAlias) {
		this.hierarchyAlias = hierarchyAlias;
	}

	public String getPtablefield() {
		return this.ptablefield;
	}

	public void setPtablefield(String ptablefield) {
		this.ptablefield = ptablefield;
	}

	public List<ChartAxis> getChartAxis() {
		return this.chartAxis;
	}

	public void setChartAxis(List<ChartAxis> chartAxis) {
		this.chartAxis = chartAxis;
	}

	public ChartAxis addChartAxi(ChartAxis chartAxi) {
		getChartAxis().add(chartAxi);
		chartAxi.setHierarchy(this);

		return chartAxi;
	}

	public ChartAxis removeChartAxi(ChartAxis chartAxi) {
		getChartAxis().remove(chartAxi);
		chartAxi.setHierarchy(null);

		return chartAxi;
	}

	public Dimension getDimension() {
		return this.dimension;
	}

	public void setDimension(Dimension dimension) {
		this.dimension = dimension;
	}

}