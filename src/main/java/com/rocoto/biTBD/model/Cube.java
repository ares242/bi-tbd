package com.rocoto.biTBD.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.List;


/**
 * The persistent class for the cube database table.
 * 
 */
@Entity
@Table(schema="metadata")
@NamedQuery(name="Cube.findAll", query="SELECT c FROM Cube c")
public class Cube implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CUBE_IDCUBE_GENERATOR", schema="metadata", sequenceName="ID_CUBE_SEQ",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CUBE_IDCUBE_GENERATOR")
	@Column(name="id_cube")
	private Long idCube;

	@Column(name="cube_alias")
	private String cubeAlias;

	@Column(name="measure_pcolumn")
	private String measurePcolumn;

	private String ptable;
	
	@Column(name="measure_name")
	private String mesureName;
	
	//bi-directional many-to-one association to Chart
	@OneToMany(mappedBy="cube")
	private List<MetaChart> charts;

	//bi-directional many-to-many association to Dimension
	@ManyToMany(mappedBy="cubes")
	private List<Dimension> dimensions;

	public Cube() {
	}

	public Long getIdCube() {
		return this.idCube;
	}

	public void setIdCube(Long idCube) {
		this.idCube = idCube;
	}

	public String getCubeAlias() {
		return this.cubeAlias;
	}

	public void setCubeAlias(String cubeAlias) {
		this.cubeAlias = cubeAlias;
	}

	public String getMeasurePcolumn() {
		return this.measurePcolumn;
	}

	public void setMeasurePcolumn(String measurePcolumn) {
		this.measurePcolumn = measurePcolumn;
	}

	public String getPtable() {
		return this.ptable;
	}

	public void setPtable(String ptable) {
		this.ptable = ptable;
	}

	public String getMesureName() {
		return mesureName;
	}

	public void setMesureName(String mesureName) {
		this.mesureName = mesureName;
	}

	public List<MetaChart> getCharts() {
		return this.charts;
	}

	public void setCharts(List<MetaChart> charts) {
		this.charts = charts;
	}

	public MetaChart addChart(MetaChart chart) {
		getCharts().add(chart);
		chart.setCube(this);

		return chart;
	}

	public MetaChart removeChart(MetaChart chart) {
		getCharts().remove(chart);
		chart.setCube(null);

		return chart;
	}

	public List<Dimension> getDimensions() {
		return this.dimensions;
	}

	public void setDimensions(List<Dimension> dimensions) {
		this.dimensions = dimensions;
	}

}