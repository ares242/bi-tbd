package com.rocoto.biTBD.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the chart_axis database table.
 * 
 */
@Embeddable
public class ChartAxiPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="id_chart", insertable=false, updatable=false)
	private Long idChart;

	@Column(name="id_dim", insertable=false, updatable=false)
	private Long idDim;

	@Column(name="id_hierarchy", insertable=false, updatable=false)
	private Long idHierarchy;

	public ChartAxiPK() {
	}
	public Long getIdChart() {
		return this.idChart;
	}
	public void setIdChart(Long idChart) {
		this.idChart = idChart;
	}
	public Long getIdDim() {
		return this.idDim;
	}
	public void setIdDim(Long idDim) {
		this.idDim = idDim;
	}
	public Long getIdHierarchy() {
		return this.idHierarchy;
	}
	public void setIdHierarchy(Long idHierarchy) {
		this.idHierarchy = idHierarchy;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof ChartAxiPK)) {
			return false;
		}
		ChartAxiPK castOther = (ChartAxiPK)other;
		return 
			this.idChart.equals(castOther.idChart)
			&& this.idDim.equals(castOther.idDim)
			&& this.idHierarchy.equals(castOther.idHierarchy);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idChart.hashCode();
		hash = hash * prime + this.idDim.hashCode();
		hash = hash * prime + this.idHierarchy.hashCode();
		
		return hash;
	}
}