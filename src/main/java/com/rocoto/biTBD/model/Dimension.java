package com.rocoto.biTBD.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Set;


/**
 * The persistent class for the dimension database table.
 * 
 */
@Entity
@NamedQuery(name="Dimension.findAll", query="SELECT d FROM Dimension d")
@Table(schema="metadata")
public class Dimension implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="DIMENSION_IDDIMENSION_GENERATOR", schema="metadata", sequenceName="ID_DIMENSION_SEQ",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="DIMENSION_IDDIMENSION_GENERATOR")
	@Column(name="id_dimension")
	private Long idDimension;

	@Column(name="dim_alias")
	private String dimAlias;

	private String ptable;
	
	@Column(name="id_pcolumn")
	private String idPcolumn;
	
	@Column(name="reference_pcolumn")
	private String referencePcolumn;
	
	//bi-directional many-to-one association to ChartAxi
	@OneToMany(mappedBy="dimension")
	private Set<ChartAxis> chartAxis;

	//bi-directional many-to-many association to Cube
	@ManyToMany
	@JoinTable(
		name="dimensions_in_cube"
		,schema="metadata"
		, joinColumns={
			@JoinColumn(name="id_dimension")
			}
		, inverseJoinColumns={
			@JoinColumn(name="id_cube")
			}
		)
	private Set<Cube> cubes;

	//bi-directional many-to-one association to Hierarchy
	@OneToMany(mappedBy="dimension")
	private Set<Hierarchy> hierarchies;

	public Dimension() {
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((chartAxis == null) ? 0 : chartAxis.hashCode());
		result = prime * result + ((cubes == null) ? 0 : cubes.hashCode());
		result = prime * result
				+ ((dimAlias == null) ? 0 : dimAlias.hashCode());
		result = prime * result
				+ ((hierarchies == null) ? 0 : hierarchies.hashCode());
		result = prime * result
				+ ((idDimension == null) ? 0 : idDimension.hashCode());
		result = prime * result
				+ ((idPcolumn == null) ? 0 : idPcolumn.hashCode());
		result = prime * result + ((ptable == null) ? 0 : ptable.hashCode());
		result = prime
				* result
				+ ((referencePcolumn == null) ? 0 : referencePcolumn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dimension other = (Dimension) obj;
		if (idDimension == null) {
			if (other.idDimension != null)
				return false;
		} else if (!idDimension.equals(other.idDimension))
			return false;
		return true;
	}

	public Long getIdDimension() {
		return this.idDimension;
	}

	public void setIdDimension(Long idDimension) {
		this.idDimension = idDimension;
	}

	public String getDimAlias() {
		return this.dimAlias;
	}

	public void setDimAlias(String dimAlias) {
		this.dimAlias = dimAlias;
	}

	public String getPtable() {
		return this.ptable;
	}

	public void setPtable(String ptable) {
		this.ptable = ptable;
	}

	public String getIdPcolumn() {
		return idPcolumn;
	}

	public void setIdPcolumn(String idPcolumn) {
		this.idPcolumn = idPcolumn;
	}

	public String getReferencePcolumn() {
		return referencePcolumn;
	}

	public void setReferencePcolumn(String referencePcolumn) {
		this.referencePcolumn = referencePcolumn;
	}

	public Set<ChartAxis> getChartAxis() {
		return this.chartAxis;
	}

	public void setChartAxis(Set<ChartAxis> chartAxis) {
		this.chartAxis = chartAxis;
	}

	public ChartAxis addChartAxi(ChartAxis chartAxi) {
		getChartAxis().add(chartAxi);
		chartAxi.setDimension(this);

		return chartAxi;
	}

	public ChartAxis removeChartAxi(ChartAxis chartAxi) {
		getChartAxis().remove(chartAxi);
		chartAxi.setDimension(null);

		return chartAxi;
	}

	public Set<Cube> getCubes() {
		return this.cubes;
	}

	public void setCubes(Set<Cube> cubes) {
		this.cubes = cubes;
	}

	public Set<Hierarchy> getHierarchies() {
		return this.hierarchies;
	}

	public void setHierarchies(Set<Hierarchy> hierarchies) {
		this.hierarchies = hierarchies;
	}

	public Hierarchy addHierarchy(Hierarchy hierarchy) {
		getHierarchies().add(hierarchy);
		hierarchy.setDimension(this);

		return hierarchy;
	}

	public Hierarchy removeHierarchy(Hierarchy hierarchy) {
		getHierarchies().remove(hierarchy);
		hierarchy.setDimension(null);

		return hierarchy;
	}

}