package com.rocoto.biTBD.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the chart database table.
 * 
 */
@Entity
@Table(name="Chart", schema="metadata")
@NamedQuery(name="MetaChart.findAll", query="SELECT c FROM MetaChart c")
public class MetaChart implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CHART_IDCHART_GENERATOR", schema="metadata" , sequenceName="ID_CHART_SEQ", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CHART_IDCHART_GENERATOR")
	@Column(name="id_chart")
	private Long idChart;

	@Column(name="chart_title")
	private String chartTitle;

	//@Column(name="id_operation")
	private String operation;

	private String sql;
	
	private String datasql;
	
	private String coment;
	
	//bi-directional many-to-one association to ChartType
	@ManyToOne
	@JoinColumn(name="id_chart_type")
	private MetaChartType chartType;

	//bi-directional many-to-one association to Cube
	@ManyToOne
	@JoinColumn(name="id_cube")
	private Cube cube;

	//bi-directional many-to-one association to ChartAxi
	@SuppressWarnings("JpaModelReferenceInspection")
    @OneToMany(mappedBy="chart")
	@OrderBy("priority ASC")
	private List<ChartAxis> chartAxis;

	public MetaChart() {
	}

	public String computeGetDataSql()
	{
		StringBuilder sql = new StringBuilder();
		StringBuilder lastColumnName = new StringBuilder();
		List<ChartAxis> axis = this.getChartAxis();
		sql.append("SELECT (p->>'").append(this.getOperation()).append("')::numeric AS ").append(this.getOperation());
		Boolean first = true, second = true;  
		for (ChartAxis ax : axis) {
			if(first)
			{
				sql.append(" , (p->>'").append(ax.getHierarchy().getPtablefield());
				sql.append("')::text AS ").append(ax.getHierarchy().getPtablefield());
				first = false;
			}
			else if(second)
			{
				sql.append(" , (p->>'").append(ax.getHierarchy().getPtablefield()).append("')::text");
				lastColumnName.append(ax.getHierarchy().getHierarchyAlias());
				second = false;
			}
			else{
				sql.append(" || '-' || (p->>'").append(ax.getHierarchy().getPtablefield()).append("')::text");
				lastColumnName.append("-").append(ax.getHierarchy().getHierarchyAlias());
			}
		}
		if(axis.size()>=2)
			sql.append(" AS \"").append(lastColumnName).append("\"");
		sql.append(" FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =").append(this.getIdChart()).append(") t1");
		return sql.toString();
	}
	
	public String computeSql() {
		List<ChartAxis> axis = this.getChartAxis();
		Set<Dimension> tables = new HashSet<>();
		StringBuilder sql = new StringBuilder();
		StringBuilder table_cols = new StringBuilder();
		boolean first = true;
		for (ChartAxis ax : axis) {
			if (!first)
				table_cols.append(',');
			else
				first = false;
			String table = ax.getDimension().getPtable();
			table_cols.append('\"' + table + '\"');
			tables.add(ax.getDimension());
			table_cols
					.append(".\"" + ax.getHierarchy().getPtablefield() + "\"");
		}

		StringBuilder joins = new StringBuilder();
		String cube_table = "\"" + this.getCube().getPtable() + "\"";
		joins.append(cube_table);
		for (Dimension d : tables) {
			String t = "\"" + d.getPtable() + "\"";
			joins.append(" INNER JOIN ").append(t);
			joins.append(" ON ").append(cube_table).append('.');
			joins.append("\"" + d.getReferencePcolumn() + "\"").append(" = ");
			joins.append(t).append('.').append("\"" + d.getIdPcolumn() + "\"");
		}
		sql.append("SELECT ").append(this.getOperation()).append('(')
				.append(cube_table)
				.append(".\"" + this.getCube().getMeasurePcolumn() + "\")")
				.append(" , ").append(table_cols).append(" FROM ");
		sql.append(joins).append(" GROUP BY ").append(table_cols);
		
		return sql.toString();
	}

    public String computeDrillDownSql( Collection<Hierarchy> newColumns, List<String> filters) {
        List<ChartAxis> axis = this.getChartAxis();
        Set<Dimension> tables = new HashSet<>();
        StringBuilder sql = new StringBuilder();
        StringBuilder table_cols = new StringBuilder();
        boolean first = true;

        for(ChartAxis ax : axis)
            tables.add(ax.getDimension());

        for (Hierarchy h : newColumns) {
            if (!first)
                table_cols.append(',');
            else
                first = false;
            String table = h.getDimension().getPtable();
            table_cols.append('\"' + table + '\"');
            //tables.add(h.getDimension());
            table_cols.append(".\"" + h.getPtablefield() + "\"");
        }

        StringBuilder joins = new StringBuilder();
        String cube_table = "\"" + this.getCube().getPtable() + "\"";
        joins.append(cube_table);
        for (Dimension d : tables) {
            String t = "\"" + d.getPtable() + "\"";
            joins.append(" INNER JOIN ").append(t);
            joins.append(" ON ").append(cube_table).append('.');
            joins.append("\"" + d.getReferencePcolumn() + "\"").append(" = ");
            joins.append(t).append('.').append("\"" + d.getIdPcolumn() + "\"");
        }

        StringBuilder filter = new StringBuilder(" WHERE");
        for(int i = 0; i<axis.size();i++){
            if(i!=0) filter.append(" AND");
            Hierarchy h = axis.get(i).getHierarchy();
            filter.append(" \"").append(h.getDimension().getPtable()).append("\".\"").append(h.getPtablefield()).append("\" = '").append(filters.get(i)).append("'");
        }

        sql.append("SELECT ").append(this.getOperation()).append('(')
                .append(cube_table)
                .append(".\"" + this.getCube().getMeasurePcolumn() + "\")")
                .append(" , ").append(table_cols).append(" FROM ");
        sql.append(joins).append(filter.toString()).append(" ").append(" GROUP BY ").append(table_cols);

        return sql.toString();
    }

	public Long getIdChart() {
		return this.idChart;
	}

	public void setIdChart(Long idChart) {
		this.idChart = idChart;
	}

	public String getChartTitle() {
		return this.chartTitle;
	}

	public void setChartTitle(String chartTitle) {
		this.chartTitle = chartTitle;
	}

	public String getOperation() {
		return this.operation;
	}

	public void setOperation(String idOperation) {
		this.operation = idOperation;
	}

	public String getSql() {
		return this.sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public String getDatasql() {
		return datasql;
	}

	public void setDatasql(String datasql) {
		this.datasql = datasql;
	}

	public String getComent() {
		return coment;
	}

	public void setComent(String coment) {
		this.coment = coment;
	}

	public MetaChartType getChartType() {
		return this.chartType;
	}

	public void setChartType(MetaChartType chartType) {
		this.chartType = chartType;
	}

	public Cube getCube() {
		return this.cube;
	}

	public void setCube(Cube cube) {
		this.cube = cube;
	}

	public List<ChartAxis> getChartAxis() {
		return this.chartAxis;
	}

	public void setChartAxis(List<ChartAxis> chartAxis) {
		this.chartAxis = chartAxis;
	}

	public ChartAxis addChartAxi(ChartAxis chartAxi) {
		getChartAxis().add(chartAxi);
		chartAxi.setChart(this);

		return chartAxi;
	}

	public ChartAxis removeChartAxi(ChartAxis chartAxi) {
		getChartAxis().remove(chartAxi);
		chartAxi.setChart(null);

		return chartAxi;
	}
}