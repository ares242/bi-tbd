package com.rocoto.biTBD.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the chart_type database table.
 * 
 */
@Entity
@Table(name="chart_type",schema="metadata")
@NamedQuery(name="ChartType.findAll", query="SELECT c FROM MetaChartType c")
public class MetaChartType implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="CHART_TYPE_IDCHARTTYPE_GENERATOR", schema="metadata", sequenceName="ID_CHART_TYPE_SEQ",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CHART_TYPE_IDCHARTTYPE_GENERATOR")
	@Column(name="id_chart_type")
	private Long idChartType;

	@Column(name="chart_type_alias")
	private String chartTypeAlias;

	private String name;
	
	private Boolean enabled;
	
	@Column(name="dim_limit")
	private Integer dimLimit;
	
	public MetaChartType() {
	}

	public Long getIdChartType() {
		return this.idChartType;
	}

	public void setIdChartType(Long idChartType) {
		this.idChartType = idChartType;
	}

	public String getChartTypeAlias() {
		return this.chartTypeAlias;
	}

	public void setChartTypeAlias(String chartTypeAlias) {
		this.chartTypeAlias = chartTypeAlias;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getDimLimit() {
		return dimLimit;
	}

	public void setDimLimit(Integer dimLimit) {
		this.dimLimit = dimLimit;
	}
}