package com.rocoto.biTBD.views;

import com.rocoto.biTBD.model.Dimension;
import com.rocoto.biTBD.model.Hierarchy;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.*;

import java.util.Collection;

/**
 * Created by jose on 03/07/14.
 */
public class DrillDownDialog extends Window {

    Button okButton = new Button("OK");
    Button cancelButton = new Button("Cancel");
    TwinColSelect select = new TwinColSelect("Herarquias");
    //JPAContainer<Hierarchy> hierars = JPAContainerFactory.make(Hierarchy.class, DBHandler.getNewEntityManager());
    BeanItemContainer<Hierarchy> hierars = new BeanItemContainer<Hierarchy>(Hierarchy.class);

    public  DrillDownDialog(Collection<Hierarchy> data){
        super("DrillDownOptions");
        VerticalLayout content = new VerticalLayout();
        HorizontalLayout buttonsLayout = new HorizontalLayout();
        buttonsLayout.addComponent(okButton);
        buttonsLayout.addComponent(cancelButton);
        content.addComponent(select);
        for(Hierarchy h :data){
            Dimension d = h.getDimension();
            for(Hierarchy h0 : d.getHierarchies())
                hierars.addBean(h0);
        }
        select.setContainerDataSource(hierars);
        select.setItemCaptionPropertyId("hierarchyAlias");
        content.addComponent(buttonsLayout);

        setContent(content);
        setModal(true);
        final Window This = this;
        okButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                UI.getCurrent().removeWindow(This);
            }
        });
        cancelButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent clickEvent) {
                UI.getCurrent().removeWindow(This);
            }
        });
    }

    public Collection<Hierarchy> getSelected(){
        return (Collection<Hierarchy>)select.getValue();
    }

    public void open(){
        UI.getCurrent().addWindow(this);
    }

    public void addOkClickListener(Button.ClickListener cll){
        okButton.addClickListener(cll);
    }

}
