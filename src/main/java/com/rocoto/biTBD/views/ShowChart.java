package com.rocoto.biTBD.views;

import com.rocoto.biTBD.ChartBuilder;
import com.rocoto.biTBD.handler.DBHandler;
import com.rocoto.biTBD.model.ChartAxis;
import com.rocoto.biTBD.model.Hierarchy;
import com.rocoto.biTBD.model.MetaChart;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.PointClickEvent;
import com.vaadin.addon.charts.PointClickListener;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

@SuppressWarnings("serial")
public class ShowChart extends CustomComponent implements View {
	
	Long idChart = 0L;
	Boolean forceUpdate = false;
	private static final Logger log = LogManager.getLogger(ShowChart.class);
	
	@Override
	public void enter(ViewChangeEvent event) {
		if(event.getParameters() != null){
	           // split at "/", add each part as a label
	           String[] msgs = event.getParameters().split("/");
	           if(msgs.length>=1&&!msgs[0].isEmpty())
	           {
	        	   idChart = Long.parseLong(msgs[0]);
	        	   forceUpdate = false;
	           }
	           if(msgs.length>=2)
	           {
	        	   	forceUpdate = msgs[1].equals("update");
	        	   	log.debug("Forcing update");
	           }
	           updateView();
	       }
	}

    public void changeChart(Chart c){
        this.setCompositionRoot(c);
    }
    
	public void updateView()
	{
		EntityManager em = DBHandler.getNewEntityManager();
		final MetaChart c = em.find(MetaChart.class, idChart);
		final ChartBuilder builder = new ChartBuilder();
		builder.setForceUpdate(forceUpdate);
		builder.setChart(c);
		Chart chart = builder.build();
        if(chart==null) return;
        final ShowChart This = this;
        chart.addPointClickListener(new PointClickListener() {
            @Override
            public void onClick(final PointClickEvent pointClickEvent) {

                final String seriesName = pointClickEvent.getSeries().getName();
                final String category = pointClickEvent.getCategory();
                List<Hierarchy> hs = new ArrayList<Hierarchy>();
                for( ChartAxis ca : c.getChartAxis())
                    hs.add(ca.getHierarchy());
                final DrillDownDialog ddd = new DrillDownDialog(hs);
                ddd.addOkClickListener(new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent clickEvent) {
                        Chart ch;
                        ch = builder.DrillDown(seriesName, category,ddd.getSelected());
                        if(ch!=null) {
                            This.changeChart(ch);
                        }
                    }
                });
                ddd.open();
                System.out.println(pointClickEvent.getSeries().getName()+":"+pointClickEvent.getCategory());

            }
        });
		this.setCompositionRoot(chart);
	}
	
	public Long getIdChart() {
		return idChart;
	}

	public void setIdChart(Long idChart) {
		this.idChart = idChart;
	}

	public Boolean getForceUpdate() {
		return forceUpdate;
	}

	public void setForceUpdate(Boolean forceUpdate) {
		this.forceUpdate = forceUpdate;
	}

	@Override
	public void forEach(Consumer<? super Component> action) {
		// TODO Auto-generated method stub

	}

	@Override
	public Spliterator<Component> spliterator() {
		// TODO Auto-generated method stub
		return null;
	}
}
