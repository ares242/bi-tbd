package com.rocoto.biTBD.views;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;

import com.rocoto.biTBD.handler.DBHandler;
import com.rocoto.biTBD.model.Hierarchy;
import com.rocoto.biTBD.model.MetaChartType;
import com.rocoto.biTBD.views.Report.ItemContainer;
import com.rocoto.biTBD.views.Report.MakeDragAndDrop;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.DragAndDropWrapper;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

@SuppressWarnings("serial")
public class ChartForm extends VerticalLayout {

	VerticalLayout data_layout;
	VerticalLayout setting_layout;
	Button make_button;
	
	HorizontalLayout dims_layout;
	HorizontalLayout meas_layout;
	JPAContainer<MetaChartType> chartTypes = JPAContainerFactory.make(MetaChartType.class, DBHandler.getNewEntityManager()); 
	ComboBox comboChartType = new ComboBox("Chart Type",chartTypes);
	TextField tfieldChartTitle = new TextField("Title");
	
	public ChartForm() {
		setImmediate(false);
		setMargin(true);
		HorizontalLayout createHorizontal = new HorizontalLayout();
		createHorizontal.setImmediate(false);
		createHorizontal.setWidth("100%");
		createHorizontal.setHeight("100%");
		createHorizontal.setMargin(true);

		data_layout = DataLayout_build();

		setting_layout = settingLayout_build();

		Panel panel = new Panel();
		panel.setContent(data_layout);
		panel.setHeight("250px");
		panel.setWidth("100%");

		createHorizontal.addComponent(panel);
		createHorizontal.addComponent(setting_layout);

		createHorizontal.setExpandRatio(panel, 5.0f);
		createHorizontal.setExpandRatio(setting_layout, 15.0f);

		make_button = new Button("Crear Gráfico");
		
		make_button.addClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				comboChartType.validate();
			}
		});
		
		tfieldChartTitle.setWidth("100%");
		
		addComponent(tfieldChartTitle);
		addComponent(createHorizontal);
		addComponent(make_button);
		
		setComponentAlignment(make_button, Alignment.MIDDLE_RIGHT);

	}
	
	public void addItemContainer(ItemContainer c)
	{
		data_layout.addComponent(c);
	}
	
	public void setSaveButtonClickListener(ClickListener cl)
	{
		make_button.addClickListener(cl);
	}
	
	private VerticalLayout settingLayout_build() {

		setting_layout = new VerticalLayout();
		setting_layout.setSizeFull();

		setting_layout.setStyleName("bi-settingArea");
		HorizontalLayout measures_layout = new HorizontalLayout();
		measures_layout.setSizeFull();
		// measures_layout.setMargin(true);

		Label measures_label = new Label("Medidas");
		measures_layout.addComponent(measures_label);

		Component measures_container = MakeDragAndDrop
				.createLayout(meas_layout = new HorizontalLayout());
		measures_container.setStyleName("bi-settingContainer");
		measures_layout.addComponent(measures_container);
		measures_layout.setExpandRatio(measures_label, 2.0f);
		measures_layout.setExpandRatio(measures_container, 18.0f);
		measures_layout.setComponentAlignment(measures_label,
				Alignment.MIDDLE_CENTER);
		measures_layout.setComponentAlignment(measures_container,
				Alignment.MIDDLE_LEFT);

		HorizontalLayout dimension_layout = new HorizontalLayout();
		dimension_layout.setSizeFull();
		Component dimension_container = MakeDragAndDrop
				.createLayout(dims_layout = new HorizontalLayout());
		dimension_container.setStyleName("bi-settingContainer");
		Label dimension_label = new Label("Dimensiones");
		dimension_layout.addComponent(dimension_label);
		dimension_layout.addComponent(dimension_container);
		dimension_layout.setExpandRatio(dimension_label, 2.0f);
		dimension_layout.setExpandRatio(dimension_container, 18.0f);
		dimension_layout.setComponentAlignment(dimension_label,
				Alignment.MIDDLE_CENTER);
		dimension_layout.setComponentAlignment(dimension_container,
				Alignment.MIDDLE_LEFT);

		setting_layout.addComponent(measures_layout);
		setting_layout.addComponent(dimension_layout);
		
		chartTypes.addContainerFilter(new Compare.Equal("enabled", true));
		comboChartType.setItemCaptionPropertyId("chartTypeAlias");
		comboChartType.addValidator(new Validator() {
			
			@Override
			public void validate(Object value) throws InvalidValueException {
				if(value == null)
					throw new InvalidValueException("Select chart type");
			}
		});
		setting_layout.addComponent(comboChartType);
		
		return setting_layout;
	}

	public VerticalLayout DataLayout_build() {
		data_layout = new VerticalLayout();
		return data_layout;
	}

	public List<Hierarchy> getChartHierarchies(){
		List<Hierarchy> dims = new ArrayList<Hierarchy>();
		Iterator<Component> iter = dims_layout.iterator();
		while(iter.hasNext())
	 	{
	 		Label l =  (Label)((DragAndDropWrapper)iter.next()).iterator().next();
	 		dims.add((Hierarchy)l.getData());
	 	}
		return dims;
	}
	
	public MetaChartType getChartType(){
		return chartTypes.getItem(comboChartType.getValue()).getEntity();
	}
	
	public String getChartTitle(){
		return tfieldChartTitle.getValue();
	}
	
	@Override
	public void forEach(Consumer<? super Component> action) {
		// TODO Auto-generated method stub

	}

	@Override
	public Spliterator<Component> spliterator() {
		// TODO Auto-generated method stub
		return null;
	}

}
