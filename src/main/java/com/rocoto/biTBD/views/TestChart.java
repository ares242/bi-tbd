package com.rocoto.biTBD.views;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.function.Consumer;

import javax.persistence.EntityManager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.rocoto.biTBD.handler.DBHandler;
import com.rocoto.biTBD.model.ChartAxis;
import com.rocoto.biTBD.model.Dimension;
import com.vaadin.addon.charts.Chart;
import com.vaadin.addon.charts.model.ChartType;
import com.vaadin.addon.charts.model.Configuration;
import com.vaadin.addon.charts.model.HorizontalAlign;
import com.vaadin.addon.charts.model.Labels;
import com.vaadin.addon.charts.model.LayoutDirection;
import com.vaadin.addon.charts.model.Legend;
import com.vaadin.addon.charts.model.ListSeries;
import com.vaadin.addon.charts.model.PlotOptionsColumn;
import com.vaadin.addon.charts.model.Series;
import com.vaadin.addon.charts.model.Tooltip;
import com.vaadin.addon.charts.model.VerticalAlign;
import com.vaadin.addon.charts.model.XAxis;
import com.vaadin.addon.charts.model.YAxis;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class TestChart extends CustomComponent implements View {

	private final VerticalLayout root_layout = new VerticalLayout();
	private static final Logger log = LogManager.getLogger(TestChart.class);
	private final HorizontalLayout data_layout = new HorizontalLayout();
	private final JPAContainer<com.rocoto.biTBD.model.MetaChart> charts_container = JPAContainerFactory
			.make(com.rocoto.biTBD.model.MetaChart.class,
					DBHandler.getNewEntityManager());
	private final Table charts_table = new Table("Charts");
	private final Button update_chart_button = new Button();
	private Chart chart;

	public TestChart() {
		update_chart_button.addClickListener(new ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				// buildChart();
			}
		});

		EntityManager em = DBHandler.getNewEntityManager();
		com.rocoto.biTBD.model.MetaChart chart_ = em.find(
				com.rocoto.biTBD.model.MetaChart.class, 1L);
		//String sql = computeSql(chart_);
		String sql = getDataQuery(chart_);
		
		System.out.println(sql);
		//Connection con = DBHandler.jdbc.getConnection();
		em.getTransaction().begin();
		Connection con = em.unwrap(java.sql.Connection.class);
		Map<String, Map<String, Double> > data = new HashMap<>();
		Set<String> categories = new HashSet<String>();
		Set<String> seriesNames = new HashSet<String>();
		try {
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			ResultSetMetaData rsmd = rs.getMetaData();
			//Integer num_cols = rsmd.getColumnCount();

			List<Double> meassures = new ArrayList<Double>();
			
			while (rs.next()) {
				categories.add(rs.getString(2));
				seriesNames.add(rs.getString(3));
				Map<String, Double> s = data.get(rs.getString(2));
				if(s==null)
					data.put(rs.getString(2) , s = new HashMap<>());
				s.put(rs.getString(3), rs.getDouble(1));
				meassures.add(rs.getDouble(1));
			}
			//System.out.println(categories);
			//System.out.println(data);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		em.getTransaction().commit();
		
		List<Series> seriesData = new ArrayList<Series>();
		
		for(String s : seriesNames)
		{
			ListSeries series = new ListSeries();
		    series.setName(s);
			for(String cat : categories)
			{
				series.addData(data.get(cat).get(s));
			}
			seriesData.add(series);
		}
		buildChart(seriesData,categories.toArray(new String[categories.size()]));
		root_layout.addComponent(chart);
		builDataLayout();

		this.setCompositionRoot(root_layout);
	}

	public void buildChart(List<Series> data, String[] cats ) {
		log.debug("building chart");
		chart = new Chart(ChartType.COLUMN);

		Configuration conf = chart.getConfiguration();

		// conf.setTitle("Total fruit consumtion, grouped by gender");
		// conf.setSubTitle("Source: WorldClimate.com");

		// String [] cat = null;

		XAxis x = new XAxis();
		x.setCategories(cats);
		
		conf.addxAxis(x);

		YAxis y = new YAxis();
		y.setMin(0);
		y.setTitle("Rainfall (mm)");
		conf.addyAxis(y);

		Legend legend = new Legend();
		legend.setLayout(LayoutDirection.VERTICAL);
		legend.setBackgroundColor("#FFFFFF");
		legend.setHorizontalAlign(HorizontalAlign.LEFT);
		legend.setVerticalAlign(VerticalAlign.TOP);
		legend.setX(100);
		legend.setY(70);
		legend.setFloating(true);
		legend.setShadow(true);
		conf.setLegend(legend);

		Tooltip tooltip = new Tooltip();
		tooltip.setFormatter("this.x +': '+ this.y +' mm'");
		conf.setTooltip(tooltip);

		/*PlotOptionsColumn plot = new PlotOptionsColumn();
		plot.setGrouping(true);
		plot.setPointPadding(0.2);
		plot.setBorderWidth(0);*/

		for(Series s : data)
			conf.addSeries(s);
		
		chart.drawChart(conf);
	}

	void builDataLayout() {
		data_layout.setSizeFull();
		root_layout.addComponent(data_layout);
		charts_table.setContainerDataSource(charts_container);
		data_layout.addComponent(charts_table);
	}
	
	String getDataQuery(com.rocoto.biTBD.model.MetaChart chart)
	{
		StringBuilder sql = new StringBuilder();
		List<ChartAxis> axis = chart.getChartAxis();
		sql.append("SELECT (p->>'").append(chart.getOperation()).append("')::numeric AS ").append(chart.getOperation());
		for (ChartAxis ax : axis) {
				sql.append(", (p->>'").append(ax.getHierarchy().getPtablefield());
				sql.append("')::text AS ").append(ax.getHierarchy().getPtablefield());
		}
		sql.append(" FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =").append(chart.getIdChart()).append(") t1");
		return sql.toString();
	}
	
	String computeSql(com.rocoto.biTBD.model.MetaChart chart) {
		List<ChartAxis> axis = chart.getChartAxis();
		Set<Dimension> tables = new HashSet<Dimension>();
		StringBuilder sql = new StringBuilder();
		StringBuilder table_cols = new StringBuilder();
		boolean first = true;
		for (ChartAxis ax : axis) {
			if (!first)
				table_cols.append(',');
			else
				first = false;
			String table = ax.getDimension().getPtable();
			table_cols.append('\"' + table + '\"');
			tables.add(ax.getDimension());
			table_cols
					.append(".\"" + ax.getHierarchy().getPtablefield() + "\"");
		}

		StringBuilder joins = new StringBuilder();
		String cube_table = "\"" + chart.getCube().getPtable() + "\"";
		joins.append(cube_table);
		for (Dimension d : tables) {
			String t = "\"" + d.getPtable() + "\"";
			joins.append(" INNER JOIN ").append(t);
			joins.append(" ON ").append(cube_table).append('.');
			joins.append("\"" + d.getReferencePcolumn() + "\"").append(" = ");
			joins.append(t).append('.').append("\"" + d.getIdPcolumn() + "\"");
		}
		sql.append("SELECT ").append(chart.getOperation()).append('(')
				.append(cube_table)
				.append(".\"" + chart.getCube().getMeasurePcolumn() + "\")")
				.append(" , ").append(table_cols).append(" FROM ");
		sql.append(joins).append(" GROUP BY ").append(table_cols);
		
		return sql.toString();
	}

	@Override
	public void forEach(Consumer<? super Component> action) {
		// TODO Auto-generated method stub

	}

	@Override
	public Spliterator<Component> spliterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if(event.getParameters() != null){
	           // split at "/", add each part as a label
	           //String[] msgs = event.getParameters().split("/");
	           /*for (String msg : msgs) {
	               //((Layout)getContent()).addComponent(new Label(msg));
	           }*/
			/*Connection con = DBHandler.jdbc.getConnection();
			Statement stmt;
			EntityManager em = DBHandler.getNewEntityManager();
			com.rocoto.biTBD.model.Chart chart_ = em.find(
					com.rocoto.biTBD.model.Chart.class, 1L);
			String sql = computeSql(chart_);
			try {
				stmt = con.createStatement();
				stmt.executeQuery("select array_to_json(array_agg(row_to_json(t))) from ( " + sql + " ) t");
				ResultSet rs = stmt.getResultSet();
				rs.next();
				String json = rs.getString(1);
				em.getTransaction().begin();
				Query q = em.createNativeQuery("update metadata.chart set data = ?::json");
				q.setParameter(1, json);
				q.executeUpdate();
				em.getTransaction().commit();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
	       }

	}

}
