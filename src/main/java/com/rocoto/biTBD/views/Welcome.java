package com.rocoto.biTBD.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
public class Welcome extends Panel implements View{

	private HorizontalLayout mainLayout;
	
	public Welcome() {
		MainLayout_build();
		setContent(mainLayout);
		// TODO add user code here
	}
	
	private void MainLayout_build(){
		mainLayout = new HorizontalLayout();
		mainLayout.setImmediate(false);
		mainLayout.setWidth("100%");
		mainLayout.setHeight("100%");
		
		// top-level component properties
		setWidth("100.0%");
		setHeight("100.0%");
		
		VerticalLayout left_layout = new VerticalLayout();
		left_layout.setSizeFull();
		VerticalLayout right_layout = new VerticalLayout();
		right_layout.setSizeFull();
		
		Label welcome_label = new Label("Bienvenido");
		welcome_label.setStyleName("bi-bigStyle");
		
		final ThemeResource ICON = new ThemeResource("next.png");
		Link next = new Link("Siguiente",
		        new ExternalResource("#!Cubes"));
		next.setStyleName("bi-nextLabel");
		next.setIcon(ICON);
		
		left_layout.addComponent(welcome_label);
		left_layout.addComponent(next);
		left_layout.setMargin(true);
		
		left_layout.setComponentAlignment(welcome_label, Alignment.BOTTOM_RIGHT);
		left_layout.setComponentAlignment(next, Alignment.TOP_RIGHT);
		
		mainLayout.addComponent(left_layout);
		mainLayout.addComponent(right_layout);
		
		mainLayout.setExpandRatio(left_layout, 18.0f);
		mainLayout.setExpandRatio(right_layout, 2.0f);
		
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}
