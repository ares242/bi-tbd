package com.rocoto.biTBD.views;

import com.rocoto.biTBD.handler.DBHandler;
import com.rocoto.biTBD.model.*;
import com.rocoto.biTBD.ui.MainUI;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.data.util.filter.Compare;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptAll;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.DragAndDropWrapper.DragStartMode;
import com.vaadin.ui.DragAndDropWrapper.WrapperTransferable;

import javax.persistence.EntityManager;

import java.util.ArrayList;

@SuppressWarnings("serial")
public class Report extends Panel implements View{

	
	private HorizontalLayout main_horizontal;
	
	private VerticalLayout dimension_layout;
	private VerticalLayout chart_layout;

	
	private Long idCube;
	private Cube cube;

	private HorizontalLayout options_layout;
	private VerticalLayout mainvertical_layout;
	
	private Window createChart_windows;
	
	private ShowChart showChart = new ShowChart();
	
	private JPAContainer<MetaChart> charts = JPAContainerFactory.make(MetaChart.class, DBHandler.getNewEntityManager());
	private Table tableCharts = new Table("Charts",charts);
	
	public Report() {
		mainvertical_layout = mainVertical_build();
		setContent(mainvertical_layout);
	
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if(event.getParameters() != null){
	           // split at "/", add each part as a label
	           String[] msgs = event.getParameters().split("/");
	           /*for (String msg : msgs) {
	               //((Layout)getContent()).addComponent(new Label(msg));
	        	   System.out.println(msg);
	           }*/
	           idCube = Long.parseLong(msgs[0]);
	           charts.removeAllContainerFilters();
	           charts.addContainerFilter(new Compare.Equal("cube.idCube", idCube));
	           //updateView();
	       }
		
	}
	
	private HorizontalLayout OptionsLayout_build(){
		options_layout = new HorizontalLayout();
		options_layout.setImmediate(false);
		options_layout.setWidth("100%");
		options_layout.setHeight("100%");		
		options_layout.setStyleName("bi-gray");
		
		Button createChart_button = new Button("Crear Gráfico");
		createChart_button.setStyleName("bi-optionbutton");
		createChart_button.addClickListener(new ClickListener() {			
			public void buttonClick(ClickEvent event) {
				createChart_windows = createChart_build();
				getUI().addWindow(createChart_windows);
			}
		});
		
		options_layout.addComponent(createChart_button);
		options_layout.setComponentAlignment(createChart_button, Alignment.MIDDLE_CENTER);
		
		return options_layout;
	}
	
	
	private void updateView(ChartForm layout)
	{
		EntityManager em = DBHandler.getNewEntityManager();
		Cube c = em.find(Cube.class, idCube);
		ItemContainer itemSet = new ItemContainer("Dimensiones");
		for(Dimension d : c.getDimensions())
		{
			String dim = d.getDimAlias();
			for(Hierarchy h : d.getHierarchies())
			{	
				itemSet.InsertItem(createLabelDimension(dim+'.'+h.getHierarchyAlias(),h));
			}
		}
		
		itemSet.ConvertToDragAndDrop();//Funcion para que se convierta en Drag and drop para mover los iconos
	
		layout.addItemContainer(itemSet);
		
		/*List<MetaChart> charts = c.getCharts();
		if(charts.isEmpty()) return;
		MetaChart ch = charts.get(0);*/
		
		//c.getCharts();
	}
	
	public VerticalLayout mainVertical_build(){
		mainvertical_layout= new VerticalLayout();
		mainvertical_layout.setImmediate(false);
		mainvertical_layout.setWidth("100%");
		mainvertical_layout.setHeight("100%");
		
		options_layout = OptionsLayout_build();

		MainHorizontal_build();
		
		mainvertical_layout.addComponent(options_layout);
		mainvertical_layout.addComponent(main_horizontal);
		
		mainvertical_layout.setExpandRatio(options_layout, 1.5f);
		mainvertical_layout.setExpandRatio(main_horizontal, 18.5f);
		 
		
		return mainvertical_layout;
	}
	
	private Window createChart_build(){
		createChart_windows = new Window("Crear un nuevo gráfico");
		//createChart_windows.setHeight("400px");
		createChart_windows.setWidth("1000px");
		createChart_windows.setStyleName("bi-createWindows");
		
		final ChartForm layout = new ChartForm();
		updateView(layout);
		layout.setSaveButtonClickListener(new ClickListener() {
			
			@Override
			public void buttonClick(ClickEvent event) {
				createChart_windows.close();
				EntityManager em = DBHandler.getNewEntityManager();
				MetaChart ch = new MetaChart();
				Cube cube = em.find(Cube.class,idCube);
				ch.setCube(cube);
				ch.setChartTitle(layout.getChartTitle());
				ch.setChartAxis(new ArrayList<ChartAxis>());
				ch.setChartType(layout.getChartType());
				///TODO dynamic operation selection
				ch.setOperation("sum");
				ch.setComent("Test chart");
				int p = 1;
				em.getTransaction().begin();
				em.persist(ch);
				for( Hierarchy h : layout.getChartHierarchies())
				{
					ChartAxis cha = new ChartAxis();
					cha.setDimension(h.getDimension());
					cha.setHierarchy(h);
					cha.setPriority(p++);
					ch.addChartAxi(cha);
					em.persist(cha);
				}
				em.getTransaction().commit();
				//MainUI.getCurrent().getNavigator().navigateTo("ShowChart/"+ch.getIdChart());
				openChart(ch.getIdChart());
			}
		});
		createChart_windows.setContent(layout);
        createChart_windows.setStyleName("bi-createChartWindow");
        createChart_windows.setResizable(false);
        createChart_windows.center();
        
		return createChart_windows;
	}
	


	public VerticalLayout dimensionLayout_build(){
		dimension_layout = new VerticalLayout();
		dimension_layout.setSizeFull();
		dimension_layout.setMargin(true);

		return dimension_layout;
	}
	
	public VerticalLayout chartLayout_build(){
		chart_layout = new VerticalLayout();
		chart_layout.setSizeFull();
		chart_layout.setMargin(true);
		
		return chart_layout;
	}
	
	
	public void MainHorizontal_build(){
		main_horizontal = new HorizontalLayout();
		main_horizontal.setImmediate(false);
		main_horizontal.setWidth("100%");
		main_horizontal.setHeight("100%");
		setWidth("100.0%");
		setHeight("100.0%");
		
		charts.addContainerFilter(new Compare.Equal("cube", this.cube));
		charts.addNestedContainerProperty("chartType.chartTypeAlias");
		tableCharts.setSelectable(true);
		tableCharts.setVisibleColumns("idChart","chartTitle","coment","chartType.chartTypeAlias");
		tableCharts.setColumnHeaders("ID","TITLE","COMENT","TYPE");
		tableCharts.addItemClickListener(new ItemClickListener() {
			
			@Override
			public void itemClick(ItemClickEvent event) {
				if(event.isDoubleClick())
				{
					openChart((Long)event.getItemId());
				}
			}
		});
		main_horizontal.addComponent(tableCharts);
		//AQUI VAN A IR LOS GRAFICOS
		
	}
	
	private void openChart(Long id){
		Window w = new Window("Show Chart", showChart);
		showChart.setIdChart(id);
		w.setWidth("800px");
		getUI().addWindow(w);
		showChart.updateView();
	}
	
	//Codigo
	/*private Component createLayout(final AbstractLayout layout) { 
		 DragAndDropWrapper dndLayout = new DragAndDropWrapper(layout);
		 dndLayout.setSizeFull();
		 dndLayout.setDropHandler(new DropHandler() {
			 @Override
			 public AcceptCriterion getAcceptCriterion() {
				 return AcceptAll.get();
			 }
		 @Override
		 public void drop(DragAndDropEvent event) {
			 	WrapperTransferable t = (WrapperTransferable) event.getTransferable();
			 	/*if(1==1){
			 		DragAndDropWrapperTyped tmp = (DragAndDropWrapperTyped)t.getSourceComponent();
			 		Label  c =  (Label)tmp.iterator().next();
			 		System.out.println(((Hierarchy)c.getData()).getIdHierarchy());
			 		/*new Notification(tmp.getType().getSimpleName(),
				 		    "test para saber que clase es",
				 		    Notification.Type.WARNING_MESSAGE, true)
				 		    .show(Page.getCurrent());
			 	}
			 	layout.addComponent(t.getSourceComponent());
			 	Iterator<Component> iter = layout.iterator();
			 	System.out.println("content :");
			 	while(iter.hasNext())
			 	{
			 		Label l =  (Label)((DragAndDropWrapper)iter.next()).iterator().next();
			 		Hierarchy h = (Hierarchy)l.getData();
			 		System.out.println(h.getIdHierarchy());
			 	}
			 }
		 });
		 return dndLayout;
		}
	*/

		private Component createLabelMeasure(String name) {
			LabelMeasure button = new LabelMeasure(name);
			 DragAndDropWrapperTyped LabelWrap = new DragAndDropWrapperTyped(button); 
			 
			 //DragAndDropWrapper LabelWrap = new DragAndDropWrapper(button);
			 LabelWrap .setDragStartMode(DragStartMode.COMPONENT);
			 LabelWrap .setSizeUndefined();
			 LabelWrap.setStyleName("bi-label-special");
			 return LabelWrap;
			 
		}
		private Component createLabelDimension(String name,Object data) {
			LabelDimension label = new LabelDimension(name);
			label.setData(data);
			 DragAndDropWrapperTyped LabelWrap = new DragAndDropWrapperTyped(label); 
			 LabelWrap .setDragStartMode(DragStartMode.COMPONENT);
			 LabelWrap .setSizeUndefined();
			 LabelWrap.setStyleName("bi-label-special");
			 return LabelWrap;	 
		}
	

	public static class DragAndDropWrapperTyped extends DragAndDropWrapper{
		
		public DragAndDropWrapperTyped(Component root) {
			super(root);
		}
	}
	
	public static class ItemContainer extends VerticalLayout{
		VerticalLayout layoutContainer;
		
		String name;
		public ItemContainer(String n) {
			name = n;
			layoutContainer = new VerticalLayout();
			this.addComponent(new Label(name));
			this.setStyleName("bi-itemContainer");
			this.setMargin(true);
		}
		public void InsertItem(Component c){
			layoutContainer.addComponent(c);
		}
		public void ConvertToDragAndDrop(){
			this.addComponent(MakeDragAndDrop.createLayout(layoutContainer));
		}
		
	}
	
	
	public class LabelMeasure extends Label{
		public LabelMeasure(String Content){
			super(Content);
		}
	}
	
	public class LabelDimension extends Label{
		public LabelDimension(String Content){
			super(Content);
		}
	}
		
	/*public class ItemContainer extends VerticalLayout{
		private static final long serialVersionUID = 1L;
		public ItemContainer(String type_label) {			
			this.addComponent(new Label(type_label));
			this.setStyleName("bi-itemContainer");
			this.setMargin(true);
			
		}
		public void addItem(Component Item_label){
			this.addComponent(Item_label);
			
		}
	}*/


	//Clase que tiene funciones para crear Drag and Drop Elements
	public static class MakeDragAndDrop extends VerticalLayout{
		public MakeDragAndDrop(){
			
		}
		static Component createLayout(final AbstractLayout layout) { 
			 DragAndDropWrapper dndLayout = new DragAndDropWrapper(layout);
			 dndLayout.setSizeFull();
			 dndLayout.setDropHandler(new DropHandler() {
				 @Override
				 public AcceptCriterion getAcceptCriterion() {
					 return AcceptAll.get();
				 }
			 @Override
			 public void drop(DragAndDropEvent event) {
				 	WrapperTransferable t = (WrapperTransferable) event.getTransferable();				 
				 	layout.addComponent(t.getSourceComponent());
				 }
			 });
			 return dndLayout;
		}
		static Component createLabelMeasure(String name) {
			Report report = new Report();
			LabelMeasure button = report.new LabelMeasure(name);
			 DragAndDropWrapperTyped LabelWrap = new DragAndDropWrapperTyped(button); 
			 LabelWrap .setDragStartMode(DragStartMode.COMPONENT);
			 LabelWrap .setSizeUndefined();
			 LabelWrap.setStyleName("bi-label-special");
			 return LabelWrap;
			 
		}
		static Component createLabelDimension(String name) {
			Report report = new Report();
			LabelDimension button = report.new LabelDimension(name);
			 DragAndDropWrapperTyped LabelWrap = new DragAndDropWrapperTyped(button); 
			 LabelWrap .setDragStartMode(DragStartMode.COMPONENT);
			 LabelWrap .setSizeUndefined();
			 LabelWrap.setStyleName("bi-label-special");
			 return LabelWrap;
			 
		}
	}
}