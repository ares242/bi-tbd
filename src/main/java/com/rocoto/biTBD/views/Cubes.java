package com.rocoto.biTBD.views;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.rocoto.biTBD.handler.DBHandler;
import com.rocoto.biTBD.model.Cube;

@SuppressWarnings("serial")
public class Cubes extends Panel implements View{

	private VerticalLayout  main_right_layout;
	private VerticalLayout  main_left_layout;
	
	private GridLayout cubes_grid;
	private HorizontalLayout main_horizontal;

	public Cubes() {
		main_horizontal = main_h_build();
		setContent(main_horizontal);
	}

	public VerticalLayout Main_right_build(){
		main_right_layout = new VerticalLayout();
		main_right_layout.setImmediate(false);
		main_right_layout.setWidth("100%");
		main_right_layout.setHeight("100%");
		
		// top-level component properties
		setWidth("100.0%");
		setHeight("100.0%");
		
		main_right_layout.setMargin(true);
		
		Label title_label = new Label("Cubos"); 
		title_label.setStyleName("bi-text");
		
		Label line = new Label("<hr/>", ContentMode.HTML);
		line.setStyleName("bi-line");
		
		cubes_grid = cubes_build();

		main_right_layout.addComponent(title_label);
		main_right_layout.addComponent(line);
		main_right_layout.addComponent(cubes_grid);
		
		main_right_layout.setExpandRatio(title_label, 2.0f);
		main_right_layout.setExpandRatio(line, 0.5f);
		main_right_layout.setExpandRatio(cubes_grid, 17.5f);
		
      
		return main_right_layout;
	}
	
	public VerticalLayout Main_left_build(){
		main_left_layout = new VerticalLayout();
		main_left_layout.setSizeFull();
		main_left_layout.setStyleName("bi-gray");
		main_left_layout.setMargin(true);
		
		return main_left_layout;
	}
	
	public GridLayout cubes_build(){
		cubes_grid = new GridLayout(5,5);
		cubes_grid.setMargin(true);
		
		final ThemeResource ICON = new ThemeResource("cubo.png");
		
		EntityManager em = DBHandler.getNewEntityManager();
		
		TypedQuery<Cube> q = em.createNamedQuery("Cube.findAll", Cube.class);
		List<Cube> cubes = q.getResultList();
		
		for(Cube c : cubes)
		{
			Link cubo1 = new Link(c.getCubeAlias(),
			        new ExternalResource("#!Report/"+c.getIdCube()));
			cubo1.setIcon(ICON);
			cubes_grid.addComponent(cubo1);
		}

		 return cubes_grid;
	}
	
	public HorizontalLayout main_h_build(){
		main_horizontal = new HorizontalLayout();
		main_horizontal.setImmediate(false);
		main_horizontal.setWidth("100%");
		main_horizontal.setHeight("100%");
		
		// top-level component properties
		setWidth("100.0%");
		setHeight("100.0%");
		
		main_left_layout = Main_left_build();
		main_right_layout = Main_right_build();
		
		main_horizontal.addComponent(main_left_layout);
		main_horizontal.addComponent(main_right_layout);
		
		main_horizontal.setExpandRatio(main_left_layout, 4.0f);
		main_horizontal.setExpandRatio(main_right_layout, 16.0f);
		
		return main_horizontal;
	}
	

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}
