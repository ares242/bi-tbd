﻿-- insert into "dimTime"(year,month,day)
--(SELECT date_part('year',days), date_part('month',days), date_part('day',days) FROM generate_series('2015-01-01'::timestamp,
  --                             '2018-12-31', '1 day') as days);

 CREATE OR REPLACE FUNCTION get_random_number(INTEGER, INTEGER) RETURNS INTEGER AS $$
 DECLARE
    start_int ALIAS FOR $1;
    end_int ALIAS FOR $2;
 BEGIN
     RETURN trunc(random() * ((end_int+1)-start_int) + start_int);
 END;
 $$ LANGUAGE 'plpgsql' STRICT;
insert into "dimShop"(shop_alias,city,province,country)
  (SELECT 'Shop_'||shop as shop_name,
	shop as city ,
	 get_random_number(1,4) as province,1 as country
  from  generate_series(1,40) as shop);

CREATE TEMPORARY TABLE types
(
	"type" smallint,
	category smallint
);

insert into types("type",category)
SELECT row_number() over () as i ,c
  from  generate_series(1,10) as t,generate_series(1,6) as c;

insert into "dimProduct"(product_description,"type")
 (select 'Product_'||p as prod,get_random_number(1,60) as t from generate_series(1,250) as p);

update "dimProduct" P set category = (select category from types T where T."type" = P."type");

drop table types;

select (select count(*) from "dimProduct") * (select count(*) from "dimShop") * (select count(*) from "dimTime") -- 29220000

select count(*) from sales

delete from "dimShop" where id_dim_shop > 40

delete from sales where id_dim_shop > 40

delete from sales

insert into sales(id_dim_time,id_dim_shop,id_dim_prod) -- T.id_dim_time,S.id_dim_shop,P.id_dim_product
(select T.id_dim_time,S.id_dim_shop,P.id_dim_product from "dimTime" T,"dimShop" S,"dimProduct" P limit 20000000)

update sales set amount=get_random_number(1000,1000000) where id_dim_time<100 and id_dim_shop = 1 and id_dim_prod = 1;

select * from sales where id_dim_time=1000 and id_dim_shop = 20 and id_dim_prod = 200

select T.shop_alias
	, P.product_description
	, DT.day
	, sum(amount) 
from 
	sales S 
	inner join "dimShop" T ON S.id_dim_shop = T.id_dim_shop 
	inner join "dimProduct" P ON S.id_dim_prod = P.id_dim_product 
	inner join "dimTime" DT ON S.id_dim_time = DT.id_dim_time 
group by P.id_dim_product,T.id_dim_shop,DT.day

select T.year, T.month, sum(amount) from sales S inner join "dimTime" T ON S.id_dim_time = T.id_dim_time group by T.year,T.month


