--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.4
-- Started on 2014-07-09 17:04:04 PET

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 119221)
-- Name: metadata; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA metadata;


ALTER SCHEMA metadata OWNER TO postgres;

--
-- TOC entry 189 (class 3079 OID 11791)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2084 (class 0 OID 0)
-- Dependencies: 189
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- TOC entry 190 (class 3079 OID 127595)
-- Name: postgres_fdw; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgres_fdw WITH SCHEMA public;


--
-- TOC entry 2085 (class 0 OID 0)
-- Dependencies: 190
-- Name: EXTENSION postgres_fdw; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgres_fdw IS 'foreign-data wrapper for remote PostgreSQL servers';


--
-- TOC entry 1550 (class 1417 OID 127599)
-- Name: data_db; Type: SERVER; Schema: -; Owner: postgres
--

CREATE SERVER data_db FOREIGN DATA WRAPPER postgres_fdw OPTIONS (
    dbname 'postgres',
    host 'localhost',
    port '5532'
);


ALTER SERVER data_db OWNER TO postgres;

SET search_path = metadata, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 175 (class 1259 OID 119266)
-- Name: chart; Type: TABLE; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE TABLE chart (
    id_chart bigint NOT NULL,
    chart_title character varying,
    id_chart_type bigint,
    id_cube bigint,
    sql character varying,
    operation character varying,
    data json,
    datasql character varying,
    coment character varying
);


ALTER TABLE metadata.chart OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 119274)
-- Name: chart_axis; Type: TABLE; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE TABLE chart_axis (
    id_chart bigint NOT NULL,
    id_dim bigint NOT NULL,
    coment character varying,
    id_hierarchy bigint NOT NULL,
    sqlfiler character varying,
    priority smallint
);


ALTER TABLE metadata.chart_axis OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 119304)
-- Name: chart_type; Type: TABLE; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE TABLE chart_type (
    id_chart_type bigint NOT NULL,
    chart_type_alias character varying,
    name character varying,
    enabled boolean DEFAULT true,
    dim_limit smallint DEFAULT 0
);


ALTER TABLE metadata.chart_type OWNER TO postgres;

--
-- TOC entry 2086 (class 0 OID 0)
-- Dependencies: 177
-- Name: COLUMN chart_type.dim_limit; Type: COMMENT; Schema: metadata; Owner: postgres
--

COMMENT ON COLUMN chart_type.dim_limit IS '0 = infinit';


--
-- TOC entry 171 (class 1259 OID 119222)
-- Name: cube; Type: TABLE; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE TABLE cube (
    id_cube bigint NOT NULL,
    cube_alias character varying,
    ptable character varying,
    measure_pcolumn character varying,
    measure_name character varying
);


ALTER TABLE metadata.cube OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 119230)
-- Name: dimension; Type: TABLE; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE TABLE dimension (
    id_dimension bigint NOT NULL,
    dim_alias character varying,
    ptable character varying,
    id_pcolumn character varying,
    reference_pcolumn character varying
);


ALTER TABLE metadata.dimension OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 119238)
-- Name: dimensions_in_cube; Type: TABLE; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE TABLE dimensions_in_cube (
    id_cube bigint NOT NULL,
    id_dimension bigint NOT NULL
);


ALTER TABLE metadata.dimensions_in_cube OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 127621)
-- Name: grouping_operation; Type: TABLE; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE TABLE grouping_operation (
    id bigint NOT NULL,
    name character varying,
    operation character varying
);


ALTER TABLE metadata.grouping_operation OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 119243)
-- Name: hierarchy; Type: TABLE; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE TABLE hierarchy (
    id_hierarchy bigint NOT NULL,
    hierarchy_alias character varying,
    id_dimesion bigint,
    ptablefield character varying
);


ALTER TABLE metadata.hierarchy OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 119371)
-- Name: id_chart_seq; Type: SEQUENCE; Schema: metadata; Owner: postgres
--

CREATE SEQUENCE id_chart_seq
    START WITH 50
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metadata.id_chart_seq OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 119369)
-- Name: id_chart_type_seq; Type: SEQUENCE; Schema: metadata; Owner: postgres
--

CREATE SEQUENCE id_chart_type_seq
    START WITH 50
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metadata.id_chart_type_seq OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 119365)
-- Name: id_cube_seq; Type: SEQUENCE; Schema: metadata; Owner: postgres
--

CREATE SEQUENCE id_cube_seq
    START WITH 50
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metadata.id_cube_seq OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 119367)
-- Name: id_dimension_seq; Type: SEQUENCE; Schema: metadata; Owner: postgres
--

CREATE SEQUENCE id_dimension_seq
    START WITH 50
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metadata.id_dimension_seq OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 119373)
-- Name: id_hierarchy_seq; Type: SEQUENCE; Schema: metadata; Owner: postgres
--

CREATE SEQUENCE id_hierarchy_seq
    START WITH 50
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metadata.id_hierarchy_seq OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- TOC entry 178 (class 1259 OID 119355)
-- Name: id_chart_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE id_chart_seq
    START WITH 50
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_chart_seq OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 119359)
-- Name: id_chart_type_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE id_chart_type_seq
    START WITH 50
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_chart_type_seq OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 119357)
-- Name: id_cube_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE id_cube_seq
    START WITH 50
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_cube_seq OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 119363)
-- Name: id_dimension_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE id_dimension_seq
    START WITH 50
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_dimension_seq OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 119361)
-- Name: id_hierarchy_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE id_hierarchy_seq
    START WITH 50
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.id_hierarchy_seq OWNER TO postgres;

SET search_path = metadata, pg_catalog;

--
-- TOC entry 2063 (class 0 OID 119266)
-- Dependencies: 175
-- Data for Name: chart; Type: TABLE DATA; Schema: metadata; Owner: postgres
--

INSERT INTO chart VALUES (4, 'test4d', 8, 1, NULL, 'sum', NULL, NULL, 'prueba 4 dimension');
INSERT INTO chart VALUES (3, 'test3d', 8, 1, 'SELECT sum("sales"."amount") , "dimTime"."year","dimShop"."country","dimProduct"."category" FROM "sales" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" INNER JOIN "dimProduct" ON "sales"."id_dim_prod" = "dimProduct"."id_dim_product" GROUP BY "dimTime"."year","dimShop"."country","dimProduct"."category"', 'sum', NULL, 'SELECT (p->>''sum'')::numeric AS sum , (p->>''year'')::text AS year , (p->>''country'')::text || ''-'' || (p->>''category'')::text AS "country-category" FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =3) t1', 'prueba 3 dimension');
INSERT INTO chart VALUES (19, 'ChartID19', 8, 1, 'SELECT sum("sales"."amount") , "dimProduct"."category","dimShop"."country" FROM "sales" INNER JOIN "dimProduct" ON "sales"."id_dim_prod" = "dimProduct"."id_dim_product" INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" GROUP BY "dimProduct"."category","dimShop"."country"', 'sum', NULL, 'SELECT (p->>''sum'')::numeric AS sum , (p->>''category'')::text AS category , (p->>''country'')::text AS "country" FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =19) t1', 'Test chart');
INSERT INTO chart VALUES (20, 'ChartID20', 8, 1, 'SELECT sum("sales"."amount") , "dimShop"."city","dimProduct"."category","dimTime"."year" FROM "sales" INNER JOIN "dimProduct" ON "sales"."id_dim_prod" = "dimProduct"."id_dim_product" INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" GROUP BY "dimShop"."city","dimProduct"."category","dimTime"."year"', 'sum', NULL, 'SELECT (p->>''sum'')::numeric AS sum , (p->>''city'')::text AS city , (p->>''category'')::text || ''-'' || (p->>''year'')::text AS "category-year" FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =20) t1', 'Test chart');
INSERT INTO chart VALUES (70, 'ChartID70', 8, 1, 'SELECT sum("sales"."amount") , "dimProduct"."id_dim_product","dimShop"."province" FROM "sales" INNER JOIN "dimProduct" ON "sales"."id_dim_prod" = "dimProduct"."id_dim_product" INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" GROUP BY "dimProduct"."id_dim_product","dimShop"."province"', 'sum', NULL, NULL, 'Test chart');
INSERT INTO chart VALUES (34, 'ChartID34', 8, 1, NULL, 'sum', NULL, NULL, 'Test chart');
INSERT INTO chart VALUES (1, 'test2d', 8, 1, 'SELECT sum("sales"."amount") , "dimTime"."year","dimShop"."id_dim_shop" FROM "sales" INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" GROUP BY "dimTime"."year","dimShop"."id_dim_shop"', 'sum', NULL, 'SELECT (p->>''sum'')::numeric AS sum, (p->>''year'')::text AS year, (p->>''id_dim_shop'')::text AS id_dim_shop FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =1) t1', 'prueba 2 dimensiones');
INSERT INTO chart VALUES (2, 'test1d', 8, 1, 'SELECT sum("sales"."amount") , "dimTime"."year" FROM "sales" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" GROUP BY "dimTime"."year"', 'sum', NULL, 'SELECT (p->>''sum'')::numeric AS sum, (p->>''year'')::text AS year FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =2) t1', 'prueba 1 dimension');
INSERT INTO chart VALUES (10, 'TestChartID10', 8, 1, 'SELECT sum("sales"."amount") , "dimTime"."month","dimProduct"."type" FROM "sales" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" INNER JOIN "dimProduct" ON "sales"."id_dim_prod" = "dimProduct"."id_dim_product" GROUP BY "dimTime"."month","dimProduct"."type"', 'sum', NULL, NULL, NULL);
INSERT INTO chart VALUES (12, 'ChartID12', 8, 1, 'SELECT sum("sales"."amount") , "dimTime"."day","dimProduct"."id_dim_product" FROM "sales" INNER JOIN "dimProduct" ON "sales"."id_dim_prod" = "dimProduct"."id_dim_product" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" GROUP BY "dimTime"."day","dimProduct"."id_dim_product"', 'sum', NULL, NULL, 'Test chart');
INSERT INTO chart VALUES (14, 'ChartID14', 8, 1, 'SELECT sum("sales"."amount") , "dimProduct"."type","dimTime"."year" FROM "sales" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" INNER JOIN "dimProduct" ON "sales"."id_dim_prod" = "dimProduct"."id_dim_product" GROUP BY "dimProduct"."type","dimTime"."year"', 'sum', NULL, 'SELECT (p->>''sum'')::numeric AS sum , (p->>''type'')::text AS type , (p->>''year'')::text AS "year" FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =14) t1', 'Test chart');
INSERT INTO chart VALUES (15, 'ChartID15', 8, 1, 'SELECT sum("sales"."amount") , "dimTime"."year","dimShop"."country" FROM "sales" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" GROUP BY "dimTime"."year","dimShop"."country"', 'sum', NULL, 'SELECT (p->>''sum'')::numeric AS sum , (p->>''year'')::text AS year , (p->>''country'')::text AS "country" FROM (SELECT json_array_elements(data) AS p FROM metadata.chart WHERE id_chart =15) t1', 'Test chart');
INSERT INTO chart VALUES (82, 'ChartID82', 5, 1, 'SELECT sum("sales"."amount") , "dimProduct"."type","dimTime"."month","dimTime"."year" FROM "sales" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" INNER JOIN "dimProduct" ON "sales"."id_dim_prod" = "dimProduct"."id_dim_product" GROUP BY "dimProduct"."type","dimTime"."month","dimTime"."year"', 'sum', NULL, NULL, 'Test chart');
INSERT INTO chart VALUES (97, '', 5, 1, 'SELECT sum("sales"."amount") , "dimTime"."year" FROM "sales" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" GROUP BY "dimTime"."year"', 'sum', NULL, NULL, 'Test chart');
INSERT INTO chart VALUES (105, '', 8, 1, 'SELECT sum("sales"."amount") , "dimTime"."year","dimShop"."city" FROM "sales" INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" GROUP BY "dimTime"."year","dimShop"."city"', 'sum', NULL, NULL, 'Test chart');


--
-- TOC entry 2064 (class 0 OID 119274)
-- Dependencies: 176
-- Data for Name: chart_axis; Type: TABLE DATA; Schema: metadata; Owner: postgres
--

INSERT INTO chart_axis VALUES (2, 1, NULL, 1, NULL, 1);
INSERT INTO chart_axis VALUES (3, 1, NULL, 1, NULL, 1);
INSERT INTO chart_axis VALUES (3, 2, NULL, 7, NULL, 2);
INSERT INTO chart_axis VALUES (3, 3, NULL, 10, NULL, 3);
INSERT INTO chart_axis VALUES (1, 1, NULL, 1, NULL, 1);
INSERT INTO chart_axis VALUES (1, 2, NULL, 4, NULL, 2);
INSERT INTO chart_axis VALUES (10, 3, NULL, 9, NULL, 2);
INSERT INTO chart_axis VALUES (10, 1, NULL, 2, NULL, 1);
INSERT INTO chart_axis VALUES (12, 3, NULL, 8, NULL, 2);
INSERT INTO chart_axis VALUES (12, 1, NULL, 3, NULL, 1);
INSERT INTO chart_axis VALUES (14, 1, NULL, 1, NULL, 2);
INSERT INTO chart_axis VALUES (14, 3, NULL, 9, NULL, 1);
INSERT INTO chart_axis VALUES (15, 2, NULL, 7, NULL, 2);
INSERT INTO chart_axis VALUES (15, 1, NULL, 1, NULL, 1);
INSERT INTO chart_axis VALUES (19, 3, NULL, 10, NULL, 1);
INSERT INTO chart_axis VALUES (19, 2, NULL, 7, NULL, 2);
INSERT INTO chart_axis VALUES (20, 2, NULL, 6, NULL, 1);
INSERT INTO chart_axis VALUES (20, 1, NULL, 1, NULL, 3);
INSERT INTO chart_axis VALUES (20, 3, NULL, 10, NULL, 2);
INSERT INTO chart_axis VALUES (70, 3, NULL, 8, NULL, 1);
INSERT INTO chart_axis VALUES (70, 2, NULL, 5, NULL, 2);
INSERT INTO chart_axis VALUES (82, 1, NULL, 1, NULL, 3);
INSERT INTO chart_axis VALUES (82, 3, NULL, 9, NULL, 1);
INSERT INTO chart_axis VALUES (82, 1, NULL, 2, NULL, 2);
INSERT INTO chart_axis VALUES (97, 1, NULL, 1, NULL, 1);
INSERT INTO chart_axis VALUES (105, 2, NULL, 6, NULL, 2);
INSERT INTO chart_axis VALUES (105, 1, NULL, 1, NULL, 1);


--
-- TOC entry 2065 (class 0 OID 119304)
-- Dependencies: 177
-- Data for Name: chart_type; Type: TABLE DATA; Schema: metadata; Owner: postgres
--

INSERT INTO chart_type VALUES (1, 'Area', 'AREA', true, 0);
INSERT INTO chart_type VALUES (2, 'Area Range', 'AREARANGE', true, 0);
INSERT INTO chart_type VALUES (3, 'Area Spline', 'AREASPLINE', true, 0);
INSERT INTO chart_type VALUES (4, 'Area Spline Range', 'AREASPLINERANGE', true, 0);
INSERT INTO chart_type VALUES (5, 'Bar', 'BAR', true, 0);
INSERT INTO chart_type VALUES (6, 'Box Plot', 'BOXPLOT', true, 0);
INSERT INTO chart_type VALUES (7, 'Bubble', 'BUBBLE', true, 0);
INSERT INTO chart_type VALUES (9, 'Column Range', 'COLUMNRANGE', true, 0);
INSERT INTO chart_type VALUES (8, 'Column', 'COLUMN', true, 0);
INSERT INTO chart_type VALUES (10, 'Error Bar', 'ERRORBAR', true, 0);
INSERT INTO chart_type VALUES (11, 'Funnel', 'FUNNEL', true, 0);
INSERT INTO chart_type VALUES (12, 'Guage', 'GUAGE', true, 0);
INSERT INTO chart_type VALUES (13, 'Line', 'LINE', true, 0);
INSERT INTO chart_type VALUES (14, 'Pie', 'PIE', true, 0);
INSERT INTO chart_type VALUES (15, 'Scatter', 'SCATTER', true, 0);
INSERT INTO chart_type VALUES (16, 'Spline', 'SPLINE', true, 0);
INSERT INTO chart_type VALUES (17, 'Water Fall', 'WATERFALL', true, 0);


--
-- TOC entry 2059 (class 0 OID 119222)
-- Dependencies: 171
-- Data for Name: cube; Type: TABLE DATA; Schema: metadata; Owner: postgres
--

INSERT INTO cube VALUES (1, 'ventas', 'sales', 'amount', 'Sales');


--
-- TOC entry 2060 (class 0 OID 119230)
-- Dependencies: 172
-- Data for Name: dimension; Type: TABLE DATA; Schema: metadata; Owner: postgres
--

INSERT INTO dimension VALUES (3, 'product', 'dimProduct', 'id_dim_product', 'id_dim_prod');
INSERT INTO dimension VALUES (1, 'time', 'dimTime', 'id_dim_time', 'id_dim_time');
INSERT INTO dimension VALUES (2, 'shop', 'dimShop', 'id_dim_shop', 'id_dim_shop');


--
-- TOC entry 2061 (class 0 OID 119238)
-- Dependencies: 173
-- Data for Name: dimensions_in_cube; Type: TABLE DATA; Schema: metadata; Owner: postgres
--

INSERT INTO dimensions_in_cube VALUES (1, 1);
INSERT INTO dimensions_in_cube VALUES (1, 2);
INSERT INTO dimensions_in_cube VALUES (1, 3);


--
-- TOC entry 2076 (class 0 OID 127621)
-- Dependencies: 188
-- Data for Name: grouping_operation; Type: TABLE DATA; Schema: metadata; Owner: postgres
--



--
-- TOC entry 2062 (class 0 OID 119243)
-- Dependencies: 174
-- Data for Name: hierarchy; Type: TABLE DATA; Schema: metadata; Owner: postgres
--

INSERT INTO hierarchy VALUES (1, 'year', 1, 'year');
INSERT INTO hierarchy VALUES (2, 'month', 1, 'month');
INSERT INTO hierarchy VALUES (3, 'day', 1, 'day');
INSERT INTO hierarchy VALUES (4, 'shop', 2, 'id_dim_shop');
INSERT INTO hierarchy VALUES (5, 'province', 2, 'province');
INSERT INTO hierarchy VALUES (6, 'city', 2, 'city');
INSERT INTO hierarchy VALUES (7, 'country', 2, 'country');
INSERT INTO hierarchy VALUES (9, 'type', 3, 'type');
INSERT INTO hierarchy VALUES (10, 'category', 3, 'category');
INSERT INTO hierarchy VALUES (8, 'product', 3, 'id_dim_product');


--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 186
-- Name: id_chart_seq; Type: SEQUENCE SET; Schema: metadata; Owner: postgres
--

SELECT pg_catalog.setval('id_chart_seq', 112, true);


--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 185
-- Name: id_chart_type_seq; Type: SEQUENCE SET; Schema: metadata; Owner: postgres
--

SELECT pg_catalog.setval('id_chart_type_seq', 247, true);


--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 183
-- Name: id_cube_seq; Type: SEQUENCE SET; Schema: metadata; Owner: postgres
--

SELECT pg_catalog.setval('id_cube_seq', 247, true);


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 184
-- Name: id_dimension_seq; Type: SEQUENCE SET; Schema: metadata; Owner: postgres
--

SELECT pg_catalog.setval('id_dimension_seq', 247, true);


--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 187
-- Name: id_hierarchy_seq; Type: SEQUENCE SET; Schema: metadata; Owner: postgres
--

SELECT pg_catalog.setval('id_hierarchy_seq', 247, true);


SET search_path = public, pg_catalog;

--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 178
-- Name: id_chart_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('id_chart_seq', 50, false);


--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 180
-- Name: id_chart_type_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('id_chart_type_seq', 50, false);


--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 179
-- Name: id_cube_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('id_cube_seq', 50, false);


--
-- TOC entry 2095 (class 0 OID 0)
-- Dependencies: 182
-- Name: id_dimension_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('id_dimension_seq', 50, false);


--
-- TOC entry 2096 (class 0 OID 0)
-- Dependencies: 181
-- Name: id_hierarchy_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('id_hierarchy_seq', 50, false);


SET search_path = metadata, pg_catalog;

--
-- TOC entry 1937 (class 2606 OID 119319)
-- Name: chart_axis_pk; Type: CONSTRAINT; Schema: metadata; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chart_axis
    ADD CONSTRAINT chart_axis_pk PRIMARY KEY (id_chart, id_dim, id_hierarchy);


--
-- TOC entry 1933 (class 2606 OID 119270)
-- Name: chart_id_pk; Type: CONSTRAINT; Schema: metadata; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chart
    ADD CONSTRAINT chart_id_pk PRIMARY KEY (id_chart);


--
-- TOC entry 1941 (class 2606 OID 119311)
-- Name: chart_type_pk; Type: CONSTRAINT; Schema: metadata; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chart_type
    ADD CONSTRAINT chart_type_pk PRIMARY KEY (id_chart_type);


--
-- TOC entry 1925 (class 2606 OID 119229)
-- Name: cube_id_pk; Type: CONSTRAINT; Schema: metadata; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cube
    ADD CONSTRAINT cube_id_pk PRIMARY KEY (id_cube);


--
-- TOC entry 1927 (class 2606 OID 119237)
-- Name: dimension_id_pk; Type: CONSTRAINT; Schema: metadata; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dimension
    ADD CONSTRAINT dimension_id_pk PRIMARY KEY (id_dimension);


--
-- TOC entry 1929 (class 2606 OID 119242)
-- Name: dimension_in_cube_id_pk; Type: CONSTRAINT; Schema: metadata; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dimensions_in_cube
    ADD CONSTRAINT dimension_in_cube_id_pk PRIMARY KEY (id_cube, id_dimension);


--
-- TOC entry 1943 (class 2606 OID 127628)
-- Name: grouping_operation_pk; Type: CONSTRAINT; Schema: metadata; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY grouping_operation
    ADD CONSTRAINT grouping_operation_pk PRIMARY KEY (id);


--
-- TOC entry 1931 (class 2606 OID 119250)
-- Name: hierarchy_id_pk; Type: CONSTRAINT; Schema: metadata; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY hierarchy
    ADD CONSTRAINT hierarchy_id_pk PRIMARY KEY (id_hierarchy);


--
-- TOC entry 1938 (class 1259 OID 119297)
-- Name: fki_chart_axis_id_dime_fk; Type: INDEX; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_chart_axis_id_dime_fk ON chart_axis USING btree (id_dim);


--
-- TOC entry 1939 (class 1259 OID 119303)
-- Name: fki_chart_axis_id_hierarchy_fk; Type: INDEX; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_chart_axis_id_hierarchy_fk ON chart_axis USING btree (id_hierarchy);


--
-- TOC entry 1934 (class 1259 OID 119317)
-- Name: fki_chart_chart_type_id_fk; Type: INDEX; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_chart_chart_type_id_fk ON chart USING btree (id_chart_type);


--
-- TOC entry 1935 (class 1259 OID 119325)
-- Name: fki_chart_id_cube_fk; Type: INDEX; Schema: metadata; Owner: postgres; Tablespace: 
--

CREATE INDEX fki_chart_id_cube_fk ON chart USING btree (id_cube);


--
-- TOC entry 1950 (class 2606 OID 119287)
-- Name: chart_axis_id_chart_fk; Type: FK CONSTRAINT; Schema: metadata; Owner: postgres
--

ALTER TABLE ONLY chart_axis
    ADD CONSTRAINT chart_axis_id_chart_fk FOREIGN KEY (id_chart) REFERENCES chart(id_chart);


--
-- TOC entry 1949 (class 2606 OID 119292)
-- Name: chart_axis_id_dime_fk; Type: FK CONSTRAINT; Schema: metadata; Owner: postgres
--

ALTER TABLE ONLY chart_axis
    ADD CONSTRAINT chart_axis_id_dime_fk FOREIGN KEY (id_dim) REFERENCES dimension(id_dimension);


--
-- TOC entry 1951 (class 2606 OID 119298)
-- Name: chart_axis_id_hierarchy_fk; Type: FK CONSTRAINT; Schema: metadata; Owner: postgres
--

ALTER TABLE ONLY chart_axis
    ADD CONSTRAINT chart_axis_id_hierarchy_fk FOREIGN KEY (id_hierarchy) REFERENCES hierarchy(id_hierarchy);


--
-- TOC entry 1947 (class 2606 OID 119312)
-- Name: chart_chart_type_id_fk; Type: FK CONSTRAINT; Schema: metadata; Owner: postgres
--

ALTER TABLE ONLY chart
    ADD CONSTRAINT chart_chart_type_id_fk FOREIGN KEY (id_chart_type) REFERENCES chart_type(id_chart_type);


--
-- TOC entry 1948 (class 2606 OID 119320)
-- Name: chart_id_cube_fk; Type: FK CONSTRAINT; Schema: metadata; Owner: postgres
--

ALTER TABLE ONLY chart
    ADD CONSTRAINT chart_id_cube_fk FOREIGN KEY (id_cube) REFERENCES cube(id_cube);


--
-- TOC entry 1944 (class 2606 OID 119256)
-- Name: dimension_in_cube_cube_fk; Type: FK CONSTRAINT; Schema: metadata; Owner: postgres
--

ALTER TABLE ONLY dimensions_in_cube
    ADD CONSTRAINT dimension_in_cube_cube_fk FOREIGN KEY (id_cube) REFERENCES cube(id_cube);


--
-- TOC entry 1945 (class 2606 OID 119261)
-- Name: dimesion_in_cube_dimension_fk; Type: FK CONSTRAINT; Schema: metadata; Owner: postgres
--

ALTER TABLE ONLY dimensions_in_cube
    ADD CONSTRAINT dimesion_in_cube_dimension_fk FOREIGN KEY (id_dimension) REFERENCES dimension(id_dimension);


--
-- TOC entry 1946 (class 2606 OID 119251)
-- Name: hierarchy_dimension_fk; Type: FK CONSTRAINT; Schema: metadata; Owner: postgres
--

ALTER TABLE ONLY hierarchy
    ADD CONSTRAINT hierarchy_dimension_fk FOREIGN KEY (id_dimesion) REFERENCES dimension(id_dimension);


--
-- TOC entry 2083 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2014-07-09 17:04:05 PET

--
-- PostgreSQL database dump complete
--

