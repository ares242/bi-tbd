﻿
create temporary table jsontest(
	id bigint,
	data json
);

insert into jsontest
select 1,array_to_json(array_agg(row_to_json(t)))
    from (
      SELECT "dimTime"."year","dimShop"."id_dim_shop" , sum("sales"."amount") FROM "sales" INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" GROUP BY "dimTime"."year","dimShop"."id_dim_shop"
    ) t

drop type bbb

create type bbb as (year bigint ,id_dim_shop bigint ,sum bigint)

SELECT (p->>'sum')::numeric AS sum, (p->>'year')::text AS year, (p->>'id_dim_shop')::text AS shop FROM (SELECT json_array_elements(data) AS p FROM metadata.chart) t1;

SELECT * FROM json_populate_recordset(null::bbb,(select "data" from jsontest where id = 1))

SELECT sum("sales"."amount") ,"dimTime"."year","dimShop"."id_dim_shop" 
FROM "sales" 
	INNER JOIN "dimShop" ON "sales"."id_dim_shop" = "dimShop"."id_dim_shop" 
	INNER JOIN "dimTime" ON "sales"."id_dim_time" = "dimTime"."id_dim_time" 
GROUP BY "dimTime"."year","dimShop"."id_dim_shop"
ORDER BY "dimShop"."id_dim_shop","dimTime"."year"